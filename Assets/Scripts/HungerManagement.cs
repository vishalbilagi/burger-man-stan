﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HungerManagement : MonoBehaviour {

    public GameObject indicator;
    public int numberOfOrders;
	// Use this for initialization
	void Start () {
        Debug.Log(transform.childCount);
        numberOfOrders = 0;
        GenerateHunger(transform.childCount);
        InvokeRepeating("Gen", 10, 5);
	}
	
    void GenerateHunger(float max)
    {
        for (int i = 0; i < max; i++)
        {
            if (Random.Range(0,5) == i%6)
            {
                GameObject feed = Instantiate(indicator, 
                                              new Vector3(transform.GetChild(i).position.x, 
                                                          transform.GetChild(i).position.y * 2, 
                                                          transform.GetChild(i).position.z),
                                              indicator.transform.rotation) as GameObject;

                Debug.Log(transform.GetChild(i).name);
                transform.GetChild(i).GetComponent<House>().setHungry();
                feed.transform.SetParent(transform.GetChild(i).transform);
                feed.transform.SetSiblingIndex(1);

                numberOfOrders++;

            }
        }
        
    }
    

	void Gen()
    {
        if (numberOfOrders < 3)
        {
            GenerateHunger(Random.Range(1, 29));
        }
    }

}
