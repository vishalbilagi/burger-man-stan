﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour {

    public bool meHungry;
    public HungerManagement manager;

    private void Awake()
    {
        meHungry = false;
        manager = GameObject.FindGameObjectWithTag("Houses").GetComponent<HungerManagement>();
    }

    public void GetFed()
    {
        meHungry = false;
        manager.numberOfOrders--;
        Destroy(transform.GetChild(1).transform.gameObject);
    }

    public void setHungry()
    {
        meHungry = true;
    }

    public bool isHungry()
    {
        return meHungry;
    }

    private void Update()
    {
        //if (!isHungry())
        //{
        //    if (transform.childCount > 1 && transform.GetChild(1).transform.tag == "Indicator")
        //        Destroy(transform.GetChild(1).transform.gameObject);
        //}

        if(transform.childCount > 1)
        {
            meHungry = true;
        }

    }
}
