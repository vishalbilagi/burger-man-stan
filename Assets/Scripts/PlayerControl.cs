﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour {

    private Rigidbody rb;
    public float m_ForwardMovementSpeed = 5f;

    public CharacterController controller;

    private Vector3 m_MoveDir;

    //x seconds boost
    public float boost = 2f;
    public float boostMultiplier;
    public bool boosting;

    public Slider Nitro;

    public GameObject burger;
    public Transform spawnPoint;

	// Use this for initialization
	void Start () {
        //rb = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
        Nitro.minValue = 0f;
        Nitro.maxValue = 2f;
	}

    
	// Update is called once per frame
	void Update () {

        m_MoveDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
               

        if(Input.GetButtonDown("RB") && boost == 2f)
        {
            boosting = true;
        }

        if (boosting)
        {
            boost -= Time.deltaTime;
            boostMultiplier = 1.5f;
        }
        else if (!boosting)
        {
            boost += Time.deltaTime/5;
            boostMultiplier = 1f;
        }
        boost = Mathf.Clamp(boost, 0.0f, 2.0f);

        Nitro.value = boost;

        if (boost == 0.0f)
        {
            boosting = false;
        }

        controller.Move(m_MoveDir.normalized * m_ForwardMovementSpeed * boostMultiplier);
        

        if (m_MoveDir != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(m_MoveDir);
        }

        if (Input.GetButtonDown("Y"))
        {
            GameObject burgerLaunch;
            burgerLaunch = GameObject.Instantiate(burger, spawnPoint.position, spawnPoint.rotation) as GameObject;
            burgerLaunch.GetComponent<Rigidbody>().AddForce(new Vector3(0, 250f, 150f), ForceMode.Force);
            Destroy(burgerLaunch, 2f);
        }
        if (Input.GetButtonDown("B"))
        {
            GameObject burgerLaunch;
            burgerLaunch = GameObject.Instantiate(burger, spawnPoint.position, spawnPoint.rotation) as GameObject;
            burgerLaunch.GetComponent<Rigidbody>().AddForce(new Vector3(150f, 250f, 0), ForceMode.Force);
            Destroy(burgerLaunch, 2f);
        }
        if (Input.GetButtonDown("A"))
        {
            GameObject burgerLaunch;
            burgerLaunch = GameObject.Instantiate(burger, spawnPoint.position, spawnPoint.rotation) as GameObject;
            burgerLaunch.GetComponent<Rigidbody>().AddForce(new Vector3(0, 250f, -150f), ForceMode.Force);
            Destroy(burgerLaunch, 2f);
        }
        if (Input.GetButtonDown("X"))
        {
            GameObject burgerLaunch;
            burgerLaunch = GameObject.Instantiate(burger, spawnPoint.position, spawnPoint.rotation) as GameObject;
            burgerLaunch.GetComponent<Rigidbody>().AddForce(new Vector3(-150f, 250f, 0), ForceMode.Force);
            Destroy(burgerLaunch, 2f);
        }

    }

}
