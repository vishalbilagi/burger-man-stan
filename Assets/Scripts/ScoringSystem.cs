﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoringSystem : MonoBehaviour {

    public int score;

    public Text textScoreUI;

    public int levelTime = 120;
    public Behaviour[] StuffToDisable;

    public Text sessionScore;
    public float sessionTime;
    public Button Restart;
    public Button Play;
    public Button Exit;

    public Button Yes;
    public Button No;

    public Vector3 initialPlayerPosition = new Vector3(2.69f,0.499f,-.08f);
    public GameObject Player;
        
    void Start () {
        score = 0;
        textScoreUI.text = score.ToString();
        Restart.gameObject.SetActive(false);
        Play.gameObject.SetActive(true);
        sessionScore.gameObject.SetActive(false);
        Yes.gameObject.SetActive(false);
        No.gameObject.SetActive(false);
        Exit.gameObject.SetActive(false);
        Time.timeScale = 0;
        for (int i = 0; i < StuffToDisable.Length; i++)
        {
            StuffToDisable[i].enabled = false;
        }
    }
	
    public void UpdateScore()
    {
        score++;
        textScoreUI.text = score.ToString();
    }

    private void Update()
    {
        sessionTime += Time.deltaTime;

        if(sessionTime > levelTime)
        {
            FinishGame();
        }
    }

    void FinishGame()
    {
        for (int i = 0; i < StuffToDisable.Length; i++)
        {
            StuffToDisable[i].enabled = false;
        }
        Time.timeScale = 0;
        sessionScore.gameObject.SetActive(true);
        sessionScore.text = score.ToString();
        Restart.gameObject.SetActive(true);

    }

    public void RestartGame()
    {
        sessionScore.text = "0";
        textScoreUI.text = "0";
        score = 0;
        sessionTime = 0;
        StartGame();
    }

    public void StartGame()
    {
        Time.timeScale = 1;
        for (int i = 0; i < StuffToDisable.Length; i++)
        {
            StuffToDisable[i].enabled = true;
        }
        Play.gameObject.SetActive(false);
        Restart.gameObject.SetActive(false);
        Exit.gameObject.SetActive(true);
        ResetPosition();
    }

    public void ExitGameDialogue()
    {
        Time.timeScale = 0;
        Yes.gameObject.SetActive(true);
        No.gameObject.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        Yes.gameObject.SetActive(false);
        No.gameObject.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private void ResetPosition()
    {
        Player.transform.position = initialPlayerPosition;
    }

}
