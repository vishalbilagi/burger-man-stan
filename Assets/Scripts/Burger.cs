﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burger : MonoBehaviour {


    public ScoringSystem scoring;

    private void Start()
    {
        scoring = GameObject.FindGameObjectWithTag("ScoringSystem").GetComponent<ScoringSystem>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "house")
        {
            if (other.GetComponent<House>().isHungry())
            {
                other.GetComponent<House>().GetFed();
                scoring.UpdateScore();
            }
          
            Destroy(gameObject);
        }
    }

}
