﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform player;
    public Vector3 offset = new Vector3(0, 7.92f, -5.15f);
    public float dampening = 2f;
    private Quaternion oldRotation;
    private Vector3 oldOffset;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        oldRotation = transform.rotation;
        oldOffset = offset;

	}
	
	// Update is called once per frame
	void Update () {


        transform.position = Vector3.Lerp(transform.position, player.position + offset, Time.deltaTime * dampening);

        if (Input.GetAxis("Accel") < 0)
        {
            offset.y = 60f;
            transform.rotation = Quaternion.LookRotation(Vector3.down);
            transform.position = Vector3.Lerp(transform.position, Vector3.zero + new Vector3(0, 60, 0), Time.deltaTime * dampening);
            
        }
        else
        {
            offset = oldOffset;
            transform.rotation = oldRotation;
            
        }
    }
}
